<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\ColorAttributeManager\Model\AssignColorAttribute;

class ExceptionLogger
{

    protected $logger;

    public function execute(string $str)
    {
        $this->log()->info($str);
    }

    protected function log()
    {
        if (!isset($this->logger)) {
            $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/attribute_color_manager_exception.log');
            $logger = new \Zend_Log();
            $this->logger = $logger->addWriter($writer);
        }

        return $this->logger;
    }

}
