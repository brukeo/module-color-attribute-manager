<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\ColorAttributeManager\Model;

class AssignColorAttributeToSimpleProducts
{

    protected \Brukeo\ColorAttributeManager\Model\AssignColorAttribute\GetProductCollection $getProductCollection;
    protected \Brukeo\ColorAttributeManager\Model\AssignColorAttribute\GetColorsFromProductSku $getColorsFromProductSku;
    protected \Brukeo\ColorAttributeManager\Model\AssignColorAttribute\GetColorAttributeOptionIds $getColorAttributeOptionIds;
    protected \Magento\Catalog\Api\ProductRepositoryInterface $productRepository;
    protected \Brukeo\ColorAttributeManager\Model\AssignColorAttribute\ExceptionLogger $exceptionLogger;

    public function __construct(
        \Brukeo\ColorAttributeManager\Model\AssignColorAttribute\GetProductCollection $getProductCollection,
        \Brukeo\ColorAttributeManager\Model\AssignColorAttribute\GetColorsFromProductSku $getColorsFromProductSku,
        \Brukeo\ColorAttributeManager\Model\AssignColorAttribute\GetColorAttributeOptionIds $getColorAttributeOptionIds,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Brukeo\ColorAttributeManager\Model\AssignColorAttribute\ExceptionLogger $exceptionLogger
    )
    {
        $this->getProductCollection = $getProductCollection;
        $this->getColorsFromProductSku = $getColorsFromProductSku;
        $this->getColorAttributeOptionIds = $getColorAttributeOptionIds;
        $this->productRepository = $productRepository;
        $this->exceptionLogger = $exceptionLogger;
    }

    public function execute()
    {
        echo "Starting process ... \n\n";
        $this->exceptionLogger->execute("\n\n");
        $errors = [];
        $products = $this->getProductCollection->execute();
        foreach ($products as $product) {
            try {
                $sku = $product->getSku();
                $colors = $this->getColorsFromProductSku->execute($sku);

                echo "Assigning color(s): " . implode(",", $colors) . " to product id - " . $product->getId() . "\n";

                $colorIds = $this->getColorAttributeOptionIds->execute($colors);
                $mainColor = reset($colorIds);

                $product->setCustomAttribute(
                    \Brukeo\ColorAttributeManager\Helper\Constants::COLOR_ATTRBIUTE_CODE,
                    $mainColor
                );

                $this->productRepository->save($product);
            } catch (\Exception $exception) {
                $this->exceptionLogger->execute($exception->getMessage());
                echo "Got an error" . $exception->getMessage() . "\n";
            }
        }

        echo "\nProcess done. \n";
    }

}
