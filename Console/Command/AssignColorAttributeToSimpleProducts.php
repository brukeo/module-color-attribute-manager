<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\ColorAttributeManager\Console\Command;

class AssignColorAttributeToSimpleProducts extends \Symfony\Component\Console\Command\Command
{

    protected \Magento\Framework\App\State $appState;
    protected \Brukeo\ColorAttributeManager\Model\AssignColorAttributeToSimpleProducts $assignColorAttributeToSimpleProducts;

    public function __construct(
        \Magento\Framework\App\State $appState,
        \Brukeo\ColorAttributeManager\Model\AssignColorAttributeToSimpleProducts $assignColorAttributeToSimpleProducts,
        string $name = null
    )
    {
        $this->appState = $appState;
        $this->assignColorAttributeToSimpleProducts = $assignColorAttributeToSimpleProducts;

        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this->setName(\Brukeo\ColorAttributeManager\Helper\Constants::ASSIGN_COLOR_ATTRIBUTE_TO_SIMPLE_PRODUCTS_COMMAND_NAME);
        $this->setDescription(\Brukeo\ColorAttributeManager\Helper\Constants::ASSIGN_COLOR_ATTRIBUTE_TO_SIMPLE_PRODUCTS_COMMAND_DESCRIPTION);

        parent::configure();
    }

    protected function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ): int
    {
        $this->appState->setAreaCode('frontend');
        $this->assignColorAttributeToSimpleProducts->execute();

        return 1;
    }

}
