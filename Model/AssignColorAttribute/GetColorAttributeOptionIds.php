<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\ColorAttributeManager\Model\AssignColorAttribute;

class GetColorAttributeOptionIds
{

    protected array $colorAttributeOptions;
    protected \Brukeo\ColorAttributeManager\Model\AssignColorAttribute\GetColorAttributeOptions $getColorAttributeOptions;

    public function __construct(
        \Brukeo\ColorAttributeManager\Model\AssignColorAttribute\GetColorAttributeOptions $getColorAttributeOptions,
    )
    {
        $this->getColorAttributeOptions = $getColorAttributeOptions;
    }

    public function execute(array $colors): array
    {
        if (empty($this->colorAttributeOptions)) {
            $this->colorAttributeOptions = $this->getColorAttributeOptions->execute();
        }

        $result = [];
        foreach ($this->colorAttributeOptions as $colorAttributeOption) {
            if (!in_array($colorAttributeOption->getLabel(), $colors)) {
                continue;
            }

            $result[] = $colorAttributeOption->getValue();
        }

        return $result;
    }

}

