<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\ColorAttributeManager\Mapper\BrukeoOldDataSource;

class MainColor
{

    protected array $mainColors = [
        'BL' => 'Biały',
        'BR' => 'Brązowy',
        'CR' => 'Czarny',
        'CW' => 'Czerwony',
        'FL' => 'Fioletowy',
        'NB' => 'Niebieski',
        'PM' => 'Pomarańczowy',
        'RZ' => 'Różowy',
        'SR' => 'Srebrny',
        'SZ' => 'Szary',
        'ZL' => 'Zielony',
        'ZY' => 'Złoty',
        'ZT' => 'Zółty',
    ];

    public function getOptionArray(): array
    {
        return $this->mainColors;
    }

    public function toOptionArray(): array
    {
        $result = [];
        foreach ($this->mainColors as $value => $label) {
            $result[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $result;
    }

}
