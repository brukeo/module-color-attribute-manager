<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\ColorAttributeManager\Model\AssignColorAttribute;

class GetColorsFromProductSku
{

    public function execute(string $sku): array
    {
        $skuChunks = explode('-', $sku);
        if (array_key_exists(3, $skuChunks)) {
            $colors = $skuChunks[3];
            if (strlen($colors) % 2 != 0) {
                throw new \Exception("Wrong color pattern for SKU: " . $sku);
            }

            return str_split($colors, 2);
        }

        throw new \Exception("Missing color in product SKU: " . $sku);
    }

}
