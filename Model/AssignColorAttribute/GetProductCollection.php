<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\ColorAttributeManager\Model\AssignColorAttribute;

class GetProductCollection
{

    protected \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder;
    protected \Magento\Catalog\Api\ProductRepositoryInterface $productRepository;

    public function __construct(
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    )
    {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->productRepository = $productRepository;
    }

    /**
     * @return \Magento\Catalog\Api\Data\ProductInterface[]
     */
    public function execute()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('type_id', 'simple')
            ->create();

        return $this->productRepository->getList($searchCriteria)->getItems();
    }

}
