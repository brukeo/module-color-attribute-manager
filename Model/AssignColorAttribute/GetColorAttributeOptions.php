<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\ColorAttributeManager\Model\AssignColorAttribute;

class GetColorAttributeOptions
{

    protected \Magento\Catalog\Model\Product\Attribute\Repository $attributeRepository;

    public function __construct(\Magento\Catalog\Model\Product\Attribute\Repository $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
    }

    public function execute()
    {
        $options = $this->attributeRepository
            ->get(\Brukeo\ColorAttributeManager\Helper\Constants::COLOR_ATTRBIUTE_CODE)
            ->getOptions();

        return $options;
    }

}

