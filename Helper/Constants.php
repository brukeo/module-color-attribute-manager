<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\ColorAttributeManager\Helper;

class Constants
{

    const COLOR_ATTRBIUTE_CODE = 'color';
    const COLOR_ATTRBIUTE_LABEL = 'Color';

    const MAIN_COLOR_ATTRBIUTE_CODE = 'main_color';

    const ASSIGN_COLOR_ATTRIBUTE_TO_SIMPLE_PRODUCTS_COMMAND_NAME = 'brukeo:products:assign_color_attribute';
    const ASSIGN_COLOR_ATTRIBUTE_TO_SIMPLE_PRODUCTS_COMMAND_DESCRIPTION = 'Assign color attribute to all simple products based on their SKU';

}
